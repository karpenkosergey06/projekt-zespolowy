using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ButtonsOnMainMenuScript : MonoBehaviour
{

	[SerializeField] private Dropdown resolutionsDropdown;
	private Resolution[] resolutions;
	private Resolution[] res;

	
	

	private void Start()
	{
		resolutions = Screen.resolutions;
		res = resolutions.Distinct().ToArray();
		string[] strResolutions = new string[res.Length];
		for (int i = 0; i < resolutions.Length; i++)
		{
			strResolutions[i] = res[i].width.ToString() + " x " + res[i].height.ToString();
		}
		resolutionsDropdown.ClearOptions();
		resolutionsDropdown.AddOptions(strResolutions.ToList());
		Screen.SetResolution(res[res.Length - 1].width, res[res.Length - 1].height, true);

	}

	public void OnPlayClick()
	{
	
		SceneManager.LoadScene("MainScene");
	}


	public void OnScreenOptionsClick()
	{
		resolutionsDropdown.gameObject.SetActive(!resolutionsDropdown.gameObject.activeSelf);
	}
	

	public void ResolutionChange()
	{
		Screen.SetResolution(res[resolutionsDropdown.value].width, res[resolutionsDropdown.value].width, true);
	}


	public void GameExit()
	{
		Application.Quit();
	
	}


}
