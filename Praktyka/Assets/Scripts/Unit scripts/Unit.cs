using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
	public UnitTemplate unitTemplate;
	public int currentHp;
	public int currentDmg;
	public int idOnScene;
	public bool isDefeated;

	public void Start()
	{
		currentHp = unitTemplate.maxHp;
		currentDmg = unitTemplate.damage;
	}
	
	public void setIdOnScene(int id)
	{
		idOnScene = id;
	}

	public int getId()
	{
		return unitTemplate.id;
	}

	public int getLvl()
	{
		return unitTemplate.lvl;
	}

	public int getMAXHP()
	{
		return unitTemplate.maxHp;
	}

	public string getName()
	{
		return unitTemplate.unitName;
	}

	public Sprite getSprite()
	{
		return unitTemplate.sprite;
	}

	public int getDamage()
	{
		return unitTemplate.damage;
	}

	public void SetDamage(int dmg)
    {
		unitTemplate.damage += dmg;
		currentDmg = unitTemplate.damage;
    }

	public int getHealPower()
	{
		return unitTemplate.healPower;
	}

	public int getMaxHealCountPerBattle()
	{
		return unitTemplate.MaxHealCountPerBattle;
	}

	public void SetMaxHP(int hp)
    {
		unitTemplate.maxHp += hp;
		currentHp = unitTemplate.maxHp;
    }

	public bool TakeDamage(int damage)
	{
		currentHp -= damage;

		if(currentHp <= 0)
		{
			return true;
		}
		else
		{
			return false;
		}    
	}

	public void Heal(int hpCount)
	{
		currentHp += hpCount;
		if(currentHp > unitTemplate.maxHp)
		{
			currentHp = unitTemplate.maxHp;
		}
		
	}

   
}
