using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSceneLoader : MonoBehaviour
{
	private int playerLD;
	int count;
	int time = 0;
	int lostCount = 0;
	private Vector3 playerPOS;
	private string battleState;

	[SerializeField] private PlayerScript playerScript;
	[SerializeField] private EnemyHolderScript holderScript;
	[SerializeField] private PlayerInventory playerInventory;
	[SerializeField] private itemsHolder itemsHolder;
	//[SerializeField] private PlayerEqupment equipment;
	[SerializeField] private InventorySlot[] slots;
	public List<int> pickedUP = new List<int>();
	public List<int> deletedItems = new List<int>();
	public List<Vector3> itemsOnScenePositions = new List<Vector3>();
	public List<GameObject> itemsOnScene = new List<GameObject>();
	public List<Vector3> positionWhereFightWasLost = new List<Vector3>();
	private void Start()
	{

		PlayerPrefs.SetString("Started", "Started");
		if (GetSavesForPlayer())
		{
			SetSavesForPlayer();
		}

	}


	private bool GetSavesForPlayer()
	{
		if (PlayerPrefs.HasKey("WasInBattle"))
		{
			if (PlayerPrefs.HasKey("PlayerLookingDirection"))
			{
				for (int i = 0; i < itemsHolder.items.Count; i++)
				{
					if (PlayerPrefs.HasKey("AddedItemToInventory" + i))
					{
						pickedUP.Add(itemsHolder.items[i].GetComponent<PickUp>().id);

					}
				}

				if (PlayerPrefs.HasKey("Times"))
					time = PlayerPrefs.GetInt("Times");

				for (int i = 0; i < time; i++)
				{
					Vector3 positions = new Vector3(
						PlayerPrefs.GetFloat("PlayerPositionForItemX" + i),
						PlayerPrefs.GetFloat("PlayerPositionForItemY" + i),
						PlayerPrefs.GetFloat("PlayerPositionForItemZ" + i)
						);

					if(positions != Vector3.right && positions != Vector3.left && positions != Vector3.forward && positions != Vector3.back)
						itemsOnScenePositions.Add(positions + playerScript.walkDir[PlayerPrefs.GetInt("PlayerLookingDirectionForItem" + i)]);
				}

		

				// searching and deleting all the same positions of item spawning 
				for (int i = 0; i < itemsOnScenePositions.Count; i++)
				{
					for (int j = i + 1; j < itemsOnScenePositions.Count; j++)
					{
						if (itemsOnScenePositions[i] == itemsOnScenePositions[j])
						{
							itemsOnScenePositions.Remove(itemsOnScenePositions[j]);
						}
					}
				}



				for (int i = 0; i < itemsHolder.items.Count; i++)
				{
					if (PlayerPrefs.HasKey("DeletedItem" + i))
					{
						deletedItems.Add(itemsHolder.items[i].GetComponent<PickUp>().id);
					}
				}

				for (int i = 0; i < itemsHolder.items.Count; i++)
				{
					if (PlayerPrefs.HasKey("NameOfTheItem" + itemsHolder.items[i].GetComponent<PickUp>().item.name))
					{

						//Debug.Log("Found");
						//if (PlayerPrefs.HasKey("DeletedItem" + i))
						//{
						// deleting from list where only items that must be deleted after laoding the scene
						//PlayerPrefs.DeleteKey("DeletedItem" + i);

						//}
						playerInventory.Add(itemsHolder.items[i].GetComponent<PickUp>().id);
					}
				}


				playerLD = PlayerPrefs.GetInt("PlayerLookingDirection");
				playerPOS.x = PlayerPrefs.GetFloat("PlayerPositionX");
				playerPOS.y = PlayerPrefs.GetFloat("PlayerPositionY");
				playerPOS.z = PlayerPrefs.GetFloat("PlayerPositionZ");
				battleState = PlayerPrefs.GetString("BattleState");


				if (battleState == "Lost")
				{
					time = PlayerPrefs.GetInt("Times");
					int hlp = time -= 1;
					positionWhereFightWasLost.Add(itemsOnScenePositions[itemsOnScenePositions.Count - 1]);
					PlayerPrefs.DeleteKey("PlayerPositionForItemX" + hlp);
					PlayerPrefs.DeleteKey("PlayerPositionForItemY" + hlp);
					PlayerPrefs.DeleteKey("PlayerPositionForItemZ" + hlp);
					PlayerPrefs.DeleteKey("layerLookingDirectionForItem" + hlp);
				}

				PlayerPrefs.Save();
				PlayerPrefs.DeleteKey("WasInBattle");
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}

	}

	private void SetSavesForPlayer()
	{
		if (battleState == "Won")
		{
			playerScript.gameObject.transform.position = playerPOS;
			playerScript.setLookDir(PlayerPrefs.GetInt("PlayerLookingDirection"));
			//Destroy(holderScript.enemies[PlayerPrefs.GetInt("MustBeDeleted" + holderScript.enemies[PlayerPrefs.GetInt("OdOnSceneOfEnemies")])]);
			if (holderScript.enemies.Count > 0)
			{
				for (int i = 0; i < holderScript.enemies.Count; ++i)
				{
					if (PlayerPrefs.HasKey("MustBeDeleted" + i))
					{
						Destroy(holderScript.enemies[PlayerPrefs.GetInt("MustBeDeleted" + i)]);
					}
				}
			}


			//one step forward after winning the fight
			//playerScript.gameObject.transform.position += playerScript.walkDir[PlayerPrefs.GetInt("PlayerLookingDirection")];
			//string a = PlayerPrefs.GetString("BattleState");

			for (int i = 0; i < positionWhereFightWasLost.Count; i++)
			{
				for (int j = 0; j < itemsOnScenePositions.Count; j++)
				{
					if (itemsOnScenePositions[j] == positionWhereFightWasLost[i])
					{
						itemsOnScenePositions.Remove(itemsOnScenePositions[j]);
					}
				}
			}


			for (int i = 0; i < itemsOnScenePositions.Count; i++)
			{
				int rnd = Random.Range(0, itemsHolder.items.Count);
				Instantiate(itemsHolder.items[rnd], itemsOnScenePositions[i], Quaternion.identity);
			}


			for (int i = 0; i < slots.Length; i++)
			{
				if (!slots[i].isEmpty)
					slots[i].UseItem();
			}

			playerInventory.listOfDeletedItems = deletedItems;
			for (int i = 0; i < deletedItems.Count; i++)
			{
				itemsHolder.items[deletedItems[i]].SetActive(false);
			}

			for (int i = 0; i < pickedUP.Count; i++)
			{
				bool isAdded = playerInventory.Add(pickedUP[i]);
				if (isAdded)
					itemsHolder.items[pickedUP[i]].SetActive(false);
			}



			Debug.Log("Loaded");

		}
		else if (battleState == "Lost")
		{
			playerScript.gameObject.transform.position = playerPOS;
			playerScript.setLookDir(PlayerPrefs.GetInt("PlayerLookingDirection"));
			//Destroy(holderScript.enemies[PlayerPrefs.GetInt("MustBeDeleted" + holderScript.enemies[PlayerPrefs.GetInt("OdOnSceneOfEnemies")])]);
			if (holderScript.enemies.Count > 0)
			{
				for (int i = 0; i < holderScript.enemies.Count; ++i)
				{
					if (PlayerPrefs.HasKey("MustBeDeleted" + i))
					{
						Destroy(holderScript.enemies[PlayerPrefs.GetInt("MustBeDeleted" + i)]);
					}
				}
			}


			//positionWhereFightWasLost.Add(itemsOnScenePositions[itemsOnScenePositions.Count - 1]);
			//for (int i = 0; i < positionWhereFightWasLost.Count; i++)
			//{
			//	for (int j = 0; j < itemsOnScenePositions.Count; i++)
			//	{
			//		if (itemsOnScenePositions[j] == positionWhereFightWasLost[i])
			//		{
			//			itemsOnScenePositions.Remove(itemsOnScenePositions[j]);
			//		}
			//	}
			//}


			for (int i = 0; i < positionWhereFightWasLost.Count; i++)
			{
				for (int j = 0; j < itemsOnScenePositions.Count; j++)
				{
					if (itemsOnScenePositions[j] == positionWhereFightWasLost[i])
					{
						itemsOnScenePositions.Remove(itemsOnScenePositions[j]);
					}
				}
			}




			for (int i = 0; i < itemsOnScenePositions.Count; i++)
			{

				int rnd = Random.Range(0, itemsHolder.items.Count);
				Instantiate(itemsHolder.items[rnd], itemsOnScenePositions[i], Quaternion.identity);
			}


			playerScript.gameObject.transform.position = playerPOS;
			if (playerLD == 0)
			{
				playerScript.setLookDir(2);
			}
			else if (playerLD == 1)
			{
				playerScript.setLookDir(3);
			}
			else if (playerLD == 2)
			{
				playerScript.setLookDir(0);
			}
			else if (playerLD == 3)
			{
				playerScript.setLookDir(1);
			}

			playerInventory.listOfDeletedItems = deletedItems;
			for (int i = 0; i < deletedItems.Count; i++)
			{
				itemsHolder.items[deletedItems[i]].SetActive(false);
			}
			for (int i = 0; i < pickedUP.Count; i++)
			{
				bool isAdded = playerInventory.Add(pickedUP[i]);
				if (isAdded)
					itemsHolder.items[pickedUP[i]].SetActive(false);
			}



		}

	}

}
